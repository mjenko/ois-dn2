var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':  
      besede.shift();
      var zasebnoSpor = besede.join(' ').split('"');
      if(zasebnoSpor[0] == '' && zasebnoSpor[2].trim() == '' && zasebnoSpor[4] == '' && zasebnoSpor.length == 5){
            this.socket.emit('zasebno', {
          oseba: zasebnoSpor[1],
          besedilo: zasebnoSpor[3]
        });
      }
      else{
        sporocilo = 'Neznan ukaz.';
      }
      break;
      
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};