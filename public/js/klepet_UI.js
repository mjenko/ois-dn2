function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').append(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    var obrazi = [/;\)/gi, /:\)/gi, /\(y\)/gi, /:\*/gi, /:\(/gi];
    var novoSporocilo = sporocilo;
    
    novoSporocilo = novoSporocilo.replace(/</gi,'&#60;');
    novoSporocilo = novoSporocilo.replace(/>/gi,'&#62;');
    
    novoSporocilo = novoSporocilo.replace(obrazi[0], '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>');
    novoSporocilo = novoSporocilo.replace(obrazi[1], '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>');
    novoSporocilo = novoSporocilo.replace(obrazi[2], '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>');
    novoSporocilo = novoSporocilo.replace(obrazi[3], '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>');
    novoSporocilo = novoSporocilo.replace(obrazi[4], '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>');
    
    klepetApp.posljiSporocilo($('#kanal').text(), novoSporocilo);
    $('#sporocila').append(divElementEnostavniTekst(novoSporocilo));

    klepetApp.posljiSporocilo($('#kanal').text().split(' ')[2], sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));

    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.kanal);
      console.log(rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  socket.on('pridruzitevVzdevek', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    var trenutneOsebe = uporabniki.besedilo.split('\n');
    for(var mestoOsebe = 0; mestoOsebe < trenutneOsebe.length; mestoOsebe++){
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(trenutneOsebe[mestoOsebe]));
    }
  });
  
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('zasebnoUspesnoOdgovor',function(rezultat) {
    $('#sporocila').append(divElementHtmlTekst(rezultat.besedilo));
  });
  
  socket.on('zasebnoOdgovor',function(rezultat) {
      $('#sporocila').append($('<div style="font-weight: bold"></div>').text(rezultat.besedilo));
  });
  
  
  
  setInterval(function() {
    socket.emit('uporabniki');
  }, 900);
  
  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});