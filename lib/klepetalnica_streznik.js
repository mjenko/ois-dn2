var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var celVzdevek = [];
var vseh = 0;
var besede;


var fs  = require('fs');
fs.readFile("/home/ubuntu/swearWords.txt",'utf-8',function(napaka,datotekaVsebina) {
      if (napaka){
        console.log(napaka);
      }
      besede = datotekaVsebina.toString('utf-8').split("\n");
});


function cenzuraStZnakov(tekst) {
  var novTekst = '';
  for(var stevec = 0; stevec < tekst.length; stevec++){
    novTekst += "*";
  }
  return novTekst; 
}


var vzdevkiId = [];



exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPosredovanjeZasebnegaSporocila(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    
    
    socket.on('uporabniki', function() {
      var trenutniUporabniki = io.sockets.clients(trenutniKanal[socket.id]);
      if (trenutniUporabniki.length > 0) {
        var vsiUporabniki = '';
        for (var mesto in trenutniUporabniki) {
          var uporabnikSocketId = trenutniUporabniki[mesto].id;
          if (mesto > 0) {
            vsiUporabniki += '\n';
          }
          vsiUporabniki += vzdevkiGledeNaSocket[uporabnikSocketId];
        }
        socket.emit('uporabniki', {besedilo: vsiUporabniki});
      }
    });
    
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};






function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  vzdevkiId.push(socket.id); 
  return stGosta + 1;
}



function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: vzdevkiGledeNaSocket[socket.id] + ' @ ' + trenutniKanal[socket.id]});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') === 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiId.push(socket.id);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete vzdevkiId[prejsnjiVzdevekIndeks];
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        
        socket.emit('pridruzitevVzdevek', {kanal: vzdevkiGledeNaSocket[socket.id] + ' @ ' + trenutniKanal[socket.id]});
        
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
       
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeZasebnegaSporocila(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki){
  socket.on('zasebno', function (sporocilo) {
    if(vzdevkiGledeNaSocket[socket.id] == sporocilo.oseba){
      socket.emit('zasebnoUspesnoOdgovor', {besedilo: 'Sporočilo ' + sporocilo.besedilo + 
      ' uporabniku z vzdevkom ' +
      sporocilo.oseba +
      ' ni bilo mogoče posredovati.',
      uspesno: false});
    }
    else{
    var indeks=uporabljeniVzdevki.indexOf(sporocilo.oseba);
      if(indeks>-1){
        console.log(indeks);
         io.sockets.socket(vzdevkiId[indeks]).emit('zasebnoOdgovor', {
          besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): '
          + sporocilo.besedilo
        });
        socket.emit('zasebnoUspesnoOdgovor',{besedilo: '(zasebno za '
        + sporocilo.oseba+'): ' + sporocilo.besedilo,
        uspesno: true});
      }
      else{
         socket.emit('zasebnoUspesnoOdgovor',{besedilo: 'Sporočilo '
         + sporocilo.besedilo + ' uporabniku z vzdevkom ' + sporocilo.oseba
         + ' ni bilo mogoče posredovati.',
         uspesno: false});
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    
    var cenzura = sporocilo.besedilo;
    for (var mesto = 0; mesto < besede.length; mesto++){
      cenzura = cenzura.replace(new RegExp('\\b'+besede[mesto].toLowerCase()+'\\b','gi') , cenzuraStZnakov(besede[mesto]));
    }
    
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + cenzura
      });
  });
}




function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete vzdevkiId[vzdevekIndeks];
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}